#bin/bash

#######################################################
############EDIT THIS ACCORDING YOUR NEEDS#############
#######################################################
#DB filenames - 
MAGENTO_MAIN_DB_NAME=hnv2_apos
MAGENTO_MAIN_DB_FILE=hnv2_apos.sql

MAGENTO_DATABUFFER_DB_NAME=hnv2_apos_databuffer
MAGENTO_DATABUFFER_DB_FILE=hnv2_apos_databuffer.sql

MAGENTO_LOG_DB_NAME=hnv2_apos_log
MAGENTO_LOG_DB_FILE=hnv2_apos_log.sql
#######################################################
#######################################################
#######################################################

GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color

#git clone --progress --verbose HN Magento and FF
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

DB_FILES_ERROR_MSG="${YELLOW}Please make sure you have the required DB files: \n${MAGENTO_MAIN_DB_FILE} \n${MAGENTO_DATABUFFER_DB_FILE} \n${MAGENTO_LOG_DB_FILE} \nin folder: $SCRIPT_DIR/../mydata/db_exports/ \nbefore continuing ${NC}\n"


#check database exports are in place
if [ ! -f $SCRIPT_DIR/../mydata/db_exports/$MAGENTO_MAIN_DB_FILE ]; then
    printf "${RED}File $SCRIPT_DIR/../mydata/db_exports/$MAGENTO_MAIN_DB_FILE not found!${NC}\n"
    echo -e $DB_FILES_ERROR_MSG
    exit
fi

if [ ! -f $SCRIPT_DIR/../mydata/db_exports/$MAGENTO_DATABUFFER_DB_FILE ]; then
    printf "${RED}File $SCRIPT_DIR/../mydata/db_exports/$MAGENTO_DATABUFFER_DB_FILE not found!${NC}\n"
    echo -e $DB_FILES_ERROR_MSG
    exit
fi

if [ ! -f $SCRIPT_DIR/../mydata/db_exports/$MAGENTO_LOG_DB_FILE ]; then
    printf "${RED}File $SCRIPT_DIR/../mydata/db_exports/$MAGENTO_LOG_DB_FILE not found!${NC}\n"
    echo -e $DB_FILES_ERROR_MSG
    exit
fi


HN_DIR=$SCRIPT_DIR/../mydata/repository
if [ ! -d $HN_DIR ]
then
    mkdir -p $HN_DIR 
fi

#magento
cd $HN_DIR
git clone --progress --verbose git@github.com:harveynichols/magento.git
cd magento
mkdir media
mkdir var

#ff
cd $HN_DIR
git clone --progress --verbose git@github.com:harveynichols/ff.git

####Ansible Galaxy Roles
#Create roles directory
ROLES_DIR=$SCRIPT_DIR/ansible/roles
if [ ! -d $ROLES_DIR ]
then
    mkdir -p $ROLES_DIR 
fi

cd $ROLES_DIR

#NGINX role
git clone --progress --verbose https://github.com/geerlingguy/ansible-role-nginx.git
mv ansible-role-nginx geerlingguy.nginx

git clone --progress --verbose https://github.com/openmicroscopy/ansible-role-nginx-ssl-selfsigned.git

#PHP role
git clone --progress --verbose https://github.com/geerlingguy/ansible-role-repo-remi.git
mv ansible-role-repo-remi geerlingguy.repo-remi

git clone --progress --verbose https://github.com/geerlingguy/ansible-role-php.git
mv ansible-role-php geerlingguy.php

git clone --progress --verbose https://github.com/geerlingguy/ansible-role-php-mysql.git
mv ansible-role-php-mysql geerlingguy.php-mysql

#php configuration
sed -i 's/php_enablerepo: ""/php_enablerepo: "remi,remi-php71"/g' geerlingguy.php-mysql/defaults/main.yml
sed -i 's/php_enablerepo: ""/php_enablerepo: "remi-php71"/g' geerlingguy.php/defaults/main.yml
sed -i 's/__php_fpm_pool_user: apache/__php_fpm_pool_user: vagrant/g' geerlingguy.php/vars/RedHat.yml
sed -i 's/__php_fpm_pool_group: apache/__php_fpm_pool_group: vagrant/g' geerlingguy.php/vars/RedHat.yml

#MySQL role
git clone --progress --verbose https://github.com/geerlingguy/ansible-role-mysql.git
mv ansible-role-mysql geerlingguy.mysql

#MySQL configuration
sed -i 's/mysql_enablerepo: ""/mysql_enablerepo: "mysql56-community"/g' geerlingguy.mysql/defaults/main.yml
sed -i 's/mysql_key_buffer_size: "256M"/mysql_key_buffer_size: "512M"/g' geerlingguy.mysql/defaults/main.yml
sed -i 's/mysql_max_allowed_packet: "64M"/mysql_max_allowed_packet: "6G"/g' geerlingguy.mysql/defaults/main.yml

#Varnish role
git clone --progress --verbose https://github.com/geerlingguy/ansible-role-varnish.git
mv ansible-role-varnish geerlingguy.varnish
#varnish change version to 5.1
sed -i 's/varnish_version: "6.1"/varnish_version: "5.1"/g' geerlingguy.varnish/defaults/main.yml

#NodeJs role
git clone --progress --verbose https://github.com/geerlingguy/ansible-role-nodejs.git
mv ansible-role-nodejs geerlingguy.nodejs

#Xdebug
git clone --progress --verbose https://github.com/geerlingguy/ansible-role-php-xdebug.git
mv ansible-role-php-xdebug geerlingguy.php-xdebug

#Redis
git clone --progress --verbose https://github.com/geerlingguy/ansible-role-redis.git
mv ansible-role-redis geerlingguy.redis
git clone --progress --verbose https://github.com/geerlingguy/ansible-role-php-redis.git
mv ansible-role-php-redis geerlingguy.php-redis

cd $SCRIPT_DIR

ansible-playbook ansible/deploy-prerequisites.yml -i ansible/hosts -u root
ansible-playbook ansible/deploy-nginx.yml -i ansible/hosts -u root
ansible-playbook ansible/deploy-php.yml -i ansible/hosts -u root
ansible-playbook ansible/deploy-mysql.yml --extra-vars "main_db_name=$MAGENTO_MAIN_DB_NAME main_db_file=$MAGENTO_MAIN_DB_FILE databuffer_db_name=$MAGENTO_DATABUFFER_DB_NAME databuffer_db_file=$MAGENTO_DATABUFFER_DB_FILE log_db_name=$MAGENTO_LOG_DB_NAME log_db_file=$MAGENTO_LOG_DB_FILE " -i ansible/hosts -u root
ansible-playbook ansible/deploy-varnish.yml -i ansible/hosts -u root
ansible-playbook ansible/deploy-nodejs.yml -i ansible/hosts -u root
ansible-playbook ansible/deploy-xdebug.yml -i ansible/hosts -u root
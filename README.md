# HN Vagrant DEV Box

Requirements

  - [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
  - [Git](https://git-scm.com/)
  - [Ansible](https://www.ansible.com/)

### Create Vagrant Box
First we need to create the Vagrant Box
```sh
cd ~/
mkdir HarveyNichols
cd HarveyNichols
mkdir mydata
vagrant init
```
Edit accordingly the following lines of Vagrantfile
```sh
  config.vm.box = "bento/centos-6.10"
  config.vm.network "private_network", ip: "192.168.33.18"
  config.vm.synced_folder "./mydata", "/vagrant_data"
```
Now run the following to create the Vagrant Box
```sh
vagrant up
```

### Vagrant Box Provisioning
##### Preperation
Copy your ssh key to Vagrant Box (password is: vagrant)
```sh
ssh-copy-id root@192.168.33.18
```
Make sure you are able to connect to Vagrant Box through ssh without password
```sh
ssh root@192.168.33.18
```
Create db_exports directory inside mydata/
```sh
cd mydata
mkdir db_exports
```
`Inside db_exports/ directory copy the 3 database dumps (e.g. hnv2_apos.sql, hnv2_apos_databuffer.sql, hnv2_apos_log.sql) required for Magento`

##### Clone current project
Clone current project in the root of your Vagrant Box
```sh
cd ~/HarveyNichols
git clone git clone git@bitbucket.org:apostolix/hn-dev-vagrant-box.git
```

Your directory structure should look like this:

------
-HarveyNichols/
&nbsp;&nbsp;&nbsp;&nbsp;-mydata/
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-db_exports/
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-hnv2_apos.sql
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-hnv2_apos_databuffer.sql
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-hnv2_apos_log.sql
&nbsp;&nbsp;&nbsp;&nbsp;-hn-dev-vagrant-box/
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-ansible/
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-install.sh
------

`Edit install.sh if you want to change the filenames for the DB name and files.`

Start the provisioning of the Vagrant Box
```sh
cd ~/HarveyNichols/hn-dev-vagrant-box
bash install
```
Grab a coffee till installation completes. The process that takes some time is the import of the Magento DB.

##### Copy .env local.xml files
Copy .env to ff/ and local.xml to magento/

##### Start nodemonn
Run inside Vagrant Box:
```sh
cd ~/HarveyNichols/hn-dev-vagrant-box
cd /vagrant_data/repository/ff
run npm nodemonn
```





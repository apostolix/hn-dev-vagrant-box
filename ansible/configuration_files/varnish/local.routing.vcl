sub choose_backend {
  if (
    # Force MA
    req.url ~ "force_ma=true"
    # Brand A-Z
    || req.url ~ "^(/en-hk/|/int/|/)brand/$"
    # Chanel exceptions (UK only)
    || req.url ~ "^/brand/chanel/$"
  ) {
    set req.http.X-Backend = "MA";
  } elseif (
    # Force WP
    req.url ~ "force_wp=true"
    # News
    || req.url ~ "^(/en-hk/|/int/|/)news"
    || req.url ~ "^(/en-hk/|/int/|/)wordpress"
    # The OXO Tower
    || req.url ~ "^(/en-hk/|/int/|/)restaurant/the-oxo-tower"
    || req.url ~ "^(/en-hk/|/int/|/)oxo"
  ) {
    set req.http.X-Backend = "WP";
  } elsif (
    # Force FF
    req.url ~ "force_ff=true"
    # Homepage
    || req.url ~ "^(/en-hk/|/int/|/)(\?.*)?$"
    # Shade Finder
    || req.url ~ "^(/en-hk/|/int/|/)shade-finder"
    # Email
    || req.url ~ "^(/en-hk/|/int/|/)email"
    # FF
    || req.url ~ "^(/en-hk/|/int/|/)ff"
    # Category Routing
    || req.url ~ "^(/en-hk/|/int/|/)womens"
    || req.url ~ "^(/en-hk/|/int/|/)mens"
    || req.url ~ "^(/en-hk/|/int/|/)beauty"
    || req.url ~ "^(/en-hk/|/int/|/)food-and-wine"
    || req.url ~ "^(/en-hk/|/int/|/)editorial"
    || req.url ~ "^(/en-hk/|/int/|/)holiday-shop"
    || req.url ~ "^(/en-hk/|/int/|/)gifts-and-home"
    || req.url ~ "^(/en-hk/|/int/|/)christmas"
    || req.url ~ "^(/en-hk/|/int/|/)wedding-shop"
    || req.url ~ "^(/en-hk/|/int/|/)new-in"
    || req.url ~ "^(/en-hk/|/int/|/)accessories"
    || req.url ~ "^(/en-hk/|/int/|/)kidswear"
    || req.url ~ "^(/en-hk/|/int/|/)shoppingparty"
    || req.url ~ "^(/en-hk/|/int/|/)shopping-party"
    || req.url ~ "^(/en-hk/|/int/|/)sale-preview"
    || req.url ~ "^(/en-hk/|/int/|/)sale"
    # Brand Routing
    || req.url ~ "^(/en-hk/|/int/|/)brand/"
    # Search Routing
    || req.url ~ "^(/en-hk/|/int/|/)catalogsearch/"
    || req.url ~ "^(/en-hk/|/int/|/)search/"
    # Other Routing
    || req.url ~ "^/assets/"
    || req.url ~ "^/static/"
    || req.url ~ "^/data/"
    || req.url ~ "^/cache/"
    # Mobile Checkout for UK and HK, not INT
    || req.url ~ "^(/en-hk/|/)checkout/cart/" && req.url !~ "/updatePost/" && req.http.X-Device == "mobile"
    || req.url ~ "^(/en-hk/|/)checkout/step/" && req.http.X-Device == "mobile"
  ) {
    set req.http.X-Backend = "FF";
  } elseif (
    # Campaign routing
    req.url ~ "^(/en-hk/|/int/|/)campaigns"
    || req.url ~ "browser-sync"
  ) {
    set req.http.X-Backend = "CP";
  } else {
    set req.http.X-Backend = "MA";
  }
}
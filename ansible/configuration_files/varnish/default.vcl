vcl 4.0;

include "/etc/varnish/backend.vcl";
include "/etc/varnish/mobile-detect.vcl";
include "/etc/varnish/local.routing.vcl";

sub vcl_recv {
  if (req.url ~ "/api/pecr/") {
    return (synth(903, "PECR"));
  }

  call devicedetect;

  call choose_backend;

  if (req.http.X-Backend == "MA") {
    set req.backend_hint = MA;
  } elsif (req.http.X-Backend == "FF") {
    set req.backend_hint = FF;
  } elsif (req.http.X-Backend == "WP") {
    set req.backend_hint = WP;
  } elsif (req.http.X-Backend == "CP") {
    set req.backend_hint = CP;
  }

  unset req.http.accept-encoding;

  return(pass);
}

sub vcl_backend_response {
  set beresp.http.X-Esi = "page";
  set beresp.do_esi = true;

  set beresp.uncacheable = true;
  return(deliver);
}

sub vcl_deliver {
  if (req.http.X-Device) {
    set resp.http.X-Device = req.http.X-Device;
  }
}

sub vcl_synth {
  if (resp.status == 903) {
    set resp.http.Set-Cookie = "hn.pecr=" + regsub(req.url, "^/api/pecr/","") + "; path=/;";
    set resp.status = 200;
  }
  return (deliver);
}
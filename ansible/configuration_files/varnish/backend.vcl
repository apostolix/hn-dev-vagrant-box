# Magento
backend MA {
  .host = "127.0.0.1";
  .port = "8000";
}
# Full Frontal
backend FF {
  .host = "127.0.0.1";
  .port = "5000";
}
# WordPress
backend WP {
  .host = "127.0.0.1";
  .port = "80";
}
# Campaigns
backend CP {
  .host = "127.0.0.1";
  .port = "3000";
}